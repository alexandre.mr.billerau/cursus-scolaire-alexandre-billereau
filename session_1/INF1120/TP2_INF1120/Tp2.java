package com.company;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Tp2 {


    static final String MESSGAGE_CHOIX_SERVICE ="\nles services sont : \n(1) service télé\n(2) service internet\n" +
            "(3) les deux services";
    static final String MSG_FORFAIT_B = "Forfait Bien – choix de 15 chaînes à la carte";
    static final String MSG_FORFAIT_T = "Forfait Très Bien– choix de 25 chaînes à la carte";
    static final String MSG_FORFAIT_E = "Forfait Excellent – choix de 35 chaînes à la carte";
    static final String MSG_FORFAIT_30 = "Internet Fibre hybride 30 - Téléchargement = 30Mb/s et téléversement = 10Mb/s";
    static final String MSG_FORFAIT_60 = "Internet Fibre hybride 60 - Téléchargement = 60Mb/s et téléversement = 10Mb/s ";
    static final String MSG_FORFAIT_120 = "Internet Fibre hybride 120 - Téléchargement = 120Mb/s et téléversement = 20Mb/s";
    static final String MSG_CHOIX_FORFAIT_TELE = "\nles forfaits télés sont : \n(B) Bien " + MSG_FORFAIT_B + "\n(T) Très bien" + MSG_FORFAIT_T + "\n(E) Excellent " + MSG_FORFAIT_E;
    static final String MSG_CHOIX_FORFAIT_INTERNET = "\nles forfaits internet sont : " + "\n(30) " + MSG_FORFAIT_30 + "\n(60) " + MSG_FORFAIT_60 + "\n(120) " + MSG_FORFAIT_120;
    static final String ADRESSE_ENTREPRISE = "2020 rue Matata, Hakuna, QC";
    static final String TELEPHONE_ENTREPRISE = "450-515-0055";
    static final float PRIX_FORFAIT_TELEPHONE_B = 15.95f;
    static final float PRIX_FORFAIT_TELEPHONE_T = 20.95f;
    static final float PRIX_FORFAIT_TELEPHONE_E = 25.95f;
    static final float PRIX_FORFAIT_INTERNET_30 = 30.95f;
    static final float PRIX_FORFAIT_INTERNET_60 = 40.95f;
    static final float PRIX_FORFAIT_INTERNET_120 = 50.95f;
    static final float TPS = 0.05f;
    static final float TVQ = 0.09975f;
    static float prixForfaitTele = 0;
    static float prixForfaitInternet = 0;
    static String nomduforfait_I = " ";
    static String nomDuForfait_T = " ";
    static int nbAbonnesAuForfaitB = 0;
    static int nbAbonnesAuForfaitT = 0;
    static int nbAbonnesAuForfaitE = 0;
    static int nbAbonnesAuForfait30 = 0;
    static int nbAbonnesAuForfait60 = 0;
    static int nbAbonnesAuForfait120 = 0;
    static int numeroFacture = 0;


    /**
     * La mèthode permet de naviguer dans le menu selon le nombre du menu choisit par le client 
     * @param nbmenu
     */

    public static void menu (int nbmenu){

            switch (nbmenu) {

                case 0:
                    System.out.println("Merci et a la prochaine ");
                    System.exit(0);
                    break;

                case 1:
                    calculeCasSErvice_1();
                    break;

                case 2:
                    calculeCasService_2();
                    break;


            }


    }

    /**
     * Cette mèthode affiche l'option 2 du menu. Donc affiche le nombre de fois que chaque service a été demandé.
     */

    public static void calculeCasService_2()
    {
        System.out.print("\n\n<------------------------------------- Télé & Internet Pour Tous ----------------------------------->\n");
        System.out.print(ADRESSE_ENTREPRISE + "         " + TELEPHONE_ENTREPRISE + "          " + date() + "\n");

        System.out.println(MSG_FORFAIT_B + "     " + nbAbonnesAuForfaitB);
        System.out.println(MSG_FORFAIT_T + "     " + nbAbonnesAuForfaitT);
        System.out.println(MSG_FORFAIT_E + "     " + nbAbonnesAuForfaitE);
        System.out.println(MSG_FORFAIT_30 + "     " + nbAbonnesAuForfait30);
        System.out.println(MSG_FORFAIT_60 + "     " + nbAbonnesAuForfait60);
        System.out.println(MSG_FORFAIT_120 + "     " + nbAbonnesAuForfait120 + "\n");
    }

    /**
     * Cette mèthode permet de calculer la facture et d'initialiser les param pour l'affichage.
     */
    public static void calculeCasSErvice_1 ()
    {
        numeroFacture = numeroFacture + 1;
        int nbService;
        String nom = validationDuNom();
        String prenom = validationDuPrenom();
        String adresse = validationAdresse();
        String telephone = validationTelephone();
        nbService = validationDuService();

        if (nbService == 1 || nbService == 3 )
        {
            char choixDuForfaitTele = choixDuForfaitTele();
            prixForfaitTele = determinePrixForfaitTele(choixDuForfaitTele);
            nomDuForfait_T = nomDuForfaitTeleChoisit(choixDuForfaitTele);
            calculNbAabonnesB(choixDuForfaitTele);
            calculNbAabonnesT(choixDuForfaitTele);
            calculNbAabonnesE(choixDuForfaitTele);
        }
        if (nbService == 2 || nbService == 3)
        {
            int choixDuForfaitInternet = choixDuForfaitInternet();
            prixForfaitInternet = dertminePrixForfaitInternet(choixDuForfaitInternet);
            nomduforfait_I = nomDuForfaitInternetChoisit(choixDuForfaitInternet);
            calculNbAabonnes30(choixDuForfaitInternet);
            calculNbAabonnes60(choixDuForfaitInternet);
            calculNbAabonnes120(choixDuForfaitInternet);
        }

        String nomDuForfaitTeleChoisit = nomDuForfait_T;
        String nomDuForfaitInternetChoisit = nomduforfait_I;

        float sousTotal = sousTotal(prixForfaitTele, prixForfaitInternet);
        float montantTvq = calculeTvq(sousTotal);
        float montantTps = calculeTps(sousTotal);
        float montantTotal = montantTotalFacture(sousTotal, montantTps, montantTvq);

        affichage(nom, prenom, adresse, telephone, prixForfaitTele,
                prixForfaitInternet, sousTotal, montantTps,
                montantTvq, montantTotal, numeroFacture,
                nomDuForfaitTeleChoisit, nomDuForfaitInternetChoisit);

    }


    /**
     * Incremente ++ le nombre de fois que ce forfait a été demandé.
     * @param choixDuForfait
     */

    public static void calculNbAabonnesB (char choixDuForfait)
    {

        if (choixDuForfait == 'B'){
            nbAbonnesAuForfaitB ++;
        }

    }

    /**
     * Incremente ++ le nombre de fois que ce forfait a été demandé.
     * @param choixDuForfait
     */
    public static void calculNbAabonnesE (char choixDuForfait)
    {

        if (choixDuForfait == 'E'){
            nbAbonnesAuForfaitE ++;
        }

    }

    /**
     * Incremente ++ le nombre de fois que ce forfait a été demandé.
     * @param choixDuForfait
     */
    public static void calculNbAabonnesT (char choixDuForfait)
    {

        if (choixDuForfait == 'T'){
            nbAbonnesAuForfaitT ++;
        }

    }

    /**
     * Incremente ++ le nombre de fois que ce forfait a été demandé.
     * @param choixDuForfait
     */

    public static void calculNbAabonnes30 (int choixDuForfait)
    {

        if (choixDuForfait == 30){
            nbAbonnesAuForfait30 ++;
        }

    }

    /**
     * Incremente ++ le nombre de fois que ce forfait a été demandé.
     * @param choixDuForfait
     */

    public static void calculNbAabonnes60 (int choixDuForfait)
    {

        if (choixDuForfait == 60){
            nbAbonnesAuForfait60 ++;
        }

    }

    /**
     * Incremente ++ le nombre de fois que ce forfait a été demandé.
     * @param choixDuForfait
     */
    public static void calculNbAabonnes120 (int choixDuForfait)
    {

        if (choixDuForfait == 120){
            nbAbonnesAuForfait120 ++;
        }

    }

    /**
     * La mèthode intialise le nom du forfait choisit pour l'affichage
     * @param choixDuForfait
     * @return Retourne le nom du forfait internet
     */

    public static String nomDuForfaitInternetChoisit(int choixDuForfait) {
        String nomduForfaitInternet = "";
        switch (choixDuForfait) {
            case 30:
                nomduForfaitInternet = MSG_FORFAIT_30;
                break;
            case 60:
                nomduForfaitInternet = MSG_FORFAIT_60;
                break;
            case 120:
                nomduForfaitInternet = MSG_FORFAIT_120;
                break;

        }
        return nomduForfaitInternet;
    }

    /**
     * Avec un switch j'initialise le nom du forfait tele pour l'affichage
     * @param choixDuForfait
     * @return Le nom du forfait télé
     */

    public static String nomDuForfaitTeleChoisit(char choixDuForfait)
    {
        String nomduForfaitTele ="";
        switch (choixDuForfait)
        {
            case 'B' :
                nomduForfaitTele = MSG_FORFAIT_B;
                break;
            case 'T':
                nomduForfaitTele = MSG_FORFAIT_T;
                break;
            case 'E':
                nomduForfaitTele = MSG_FORFAIT_T;
                break;

        }

        return nomduForfaitTele;
    }

    /**
     * La Mèthode affiche la facture.
     * @param nom
     * @param prenom
     * @param adresse
     * @param telephone
     * @param prixForfaitTele
     * @param prixForfaitInternet
     * @param sousTotal
     * @param montantTps
     * @param montantTvq
     * @param montantTotal
     */

    public static void affichage (String nom , String prenom , String adresse, String telephone, float prixForfaitTele
    , float prixForfaitInternet, float sousTotal, float montantTps, float montantTvq, float montantTotal, int nbfacture
    , String nomDuForfaitTeleChoisit, String nomDuForfaitInternetChoisit){

        System.out.print("\n\n<------------------------------------- Télé & Internet Pour Tous ----------------------------------->\n");
        System.out.print(ADRESSE_ENTREPRISE + "                                                  " + TELEPHONE_ENTREPRISE + "\n");
        System.out.println(date() + "                                                                 " + "nb facture : " +nbfacture + "\n");
        System.out.print("Nom et Prénom : " +nom + prenom + "\n");
        System.out.print("Adresse : " + adresse + "                       Télephone : " + telephone + "\n");
        System.out.print("Description                                                            prix\n");
        System.out.println(nomDuForfaitTeleChoisit + "       " + prixForfaitTele + "\n" + nomDuForfaitInternetChoisit + "       " + prixForfaitInternet + "\n");
        System.out.printf("Sous-total : %3.2f", sousTotal);
        System.out.printf("\nMontant TPS : %3.2f", montantTps);
        System.out.printf("\nMontant TVQ : %3.2f", montantTvq);
        System.out.printf("\nMontant total : %3.2f", montantTotal);
        System.out.print("\nMerci d'avoir choisi TIPT !\n\n\n");

    }


    /**
     *
     * @return Retourne la date
     */

    public static String date ()
    {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d-MM-yyyy HH:mm:ss");
        String laDate = simpleDateFormat.format(date);
        return laDate;
    }

    /**
     * La mèthode calcule l'addition du sous total, montant tps et montant tvq
     * @param sousTotal
     * @param montantTps
     * @param montantTvq
     * @return Retourne le total de la facture
     */

    public static float montantTotalFacture(float sousTotal, float montantTps, float montantTvq)
    {

        float totalFacture = sousTotal + montantTps + montantTvq;
        return totalFacture;

    }

    /**
     * La mèthode calcule le sous total (en param) * la constant TVQ
     * @param sousTotal
     * @return Retourne le montant TVQ du sous total
     */

    public static float calculeTvq (float sousTotal)
    {
        float montantTvq = sousTotal * TVQ;
        return montantTvq;

    }

    /**
     * La mèthode calcule le sous total (en param) fois la constant TPS.
     * @param sousTotal
     * @return Retourne le montant TPS du sous total
     */

    public static float calculeTps(float sousTotal)
    {

        float montantTps = sousTotal * TPS;

        return montantTps;
    }

    /**
     * la methode calcule l'adittion des deux parametre qui sont les prix.
     * @param prixForfaitTele
     * @param prixForfaitInternet
     * @return REtourne le sous-total
     */

    public static float sousTotal(float prixForfaitTele, float prixForfaitInternet)
    {
        float sousTotal;
        sousTotal = prixForfaitTele + prixForfaitInternet;

        return sousTotal;

    }

    /**
     * Avec un swith on affecte le prix a la varible prix selon le sigle en param.
     * @param choixForfait c'est le sigle du forfait 30,60 ou 120.
     * @return Retourne le prix du forfait internet choisit.
     */

    public static float dertminePrixForfaitInternet(int choixForfait) //question int et char + si c'est une bonne idee comme methode.
    {
        float prix = 0f;
        switch (choixForfait)
        {
            case 30 :
                prix = PRIX_FORFAIT_INTERNET_30;
                break;
            case 60 :
                prix = PRIX_FORFAIT_INTERNET_60;
                break;
            case 120 :
                prix = PRIX_FORFAIT_INTERNET_120;
                break;
        }

        return prix;

    }

    /**
     * Avec un swith on affecte le prix a la varible prix selon le sigle en param.
     * @param choixForfait Le sigle du forfait Télé B,T ou E.
     * @return retourne le prix du forfait choisit .
     */

    public static float determinePrixForfaitTele(char choixForfait)
    {

        float prix = 0f;
        switch (choixForfait)
        {
            case 'B' :
                prix = PRIX_FORFAIT_TELEPHONE_B;
                break;
            case 'T' :
                prix = PRIX_FORFAIT_TELEPHONE_T;
                break;
            case 'E' :
                prix = PRIX_FORFAIT_TELEPHONE_E;
                break;
        }

        return prix;

    }

    /**
     * La méthode demande la saisie du forfait après la saisie, la lèttre est mit en majuscule et enfin test d'erreur et
     * si  il y'a erreur redemande la saisie.
     * @return Retourne le forfait choisit par le client 30,60 ou 120
     */

    public static int choixDuForfaitInternet()
    {

        System.out.print(MSG_CHOIX_FORFAIT_INTERNET);
        int nbDuForfait;

        do {
            System.out.print("\nEntrer choix du forfait internet : ");
            nbDuForfait = Clavier.lireInt();

            if (nbDuForfait != 30 && nbDuForfait != 60 && nbDuForfait != 120)
            {

                System.out.print("\nIdentifiant d’Internet invalide!");

            }


        }while (nbDuForfait != 30 && nbDuForfait != 60 && nbDuForfait != 120);

        return nbDuForfait;
    }

    /**
     * La méthode demande la saisie du forfait après la saisie, la lèttre est mit en majuscule et enfin test d'erreur et
     * si il y'a erreur redemande la saisie.
     * @return Retourne la lettre du forfait télé choisit B,T ou E
     */


    public static char choixDuForfaitTele()
    {

        System.out.print(MSG_CHOIX_FORFAIT_TELE);

        char lettreDuForfait;

        do {

            System.out.print("\nEntrer votre choix de forfait : ");
            lettreDuForfait = Clavier.lireCharLn();
            lettreDuForfait = Character.toUpperCase(lettreDuForfait);

            if (lettreDuForfait != 'B' && lettreDuForfait != 'T' && lettreDuForfait != 'E' )
            {

                System.out.print("\nIdentifiant de Télé invalide!");

            }

        }while (lettreDuForfait != 'B' && lettreDuForfait != 'T' && lettreDuForfait != 'E');

        return lettreDuForfait;

    }

    /**
     * Dans une boucle on verifie si le choix est bien entre 1 et 3 inclut sinon, message d'erreur et
     * on redemande la saisie.
     * @return retourne le choix du service 1,2 ou 3 (forfait tele, internet ou les deux)
     */

    public static int validationDuService ()
    {

        System.out.print(MESSGAGE_CHOIX_SERVICE);
        int nbService;

        do {
            System.out.print("\nChoisir votre service : ");
            nbService = Clavier.lireInt();

            if (nbService < 1 || nbService > 3)
            {

                System.out.print("\nChoix du service invalide!");

            }


        }while ( nbService != 1 && nbService !=2 && nbService !=3);

        return nbService;

    }

    /**
     *verifie si l'adress est entre 30 et 80 caract̀é̀res, sinon envoie un message d'erreur et on redemande la saisie.
     * @return l'adresse du client !
     */
    public static String validationAdresse()
    {

        String adresse = "";
        int nbChara;

        do {
            System.out.print("\nEntrer votre adresse : ");
            adresse = Clavier.lireString();
            nbChara = adresse.length();

            if (nbChara < 30 || nbChara > 80)
            {

                System.out.print("\nAdresse de l’abonné invalide!");

            }

        }while (nbChara < 30 || nbChara > 80);
        return adresse;

    }

    /**
     * Vérifie avec une expresion regulière si le numero est true, si il est fauls alors sa retourne un message
     * d'erreur et on redemande la saisie
     * @return retourne le numéro de téléphone
     */

    public static String validationTelephone()
    {

        String numero = "";
        boolean numValide = false;

        do {
            System.out.print("\nEntrer votre numéro de téléphone : ");
            numero = Clavier.lireString();

            if (numero.matches("^\\(?([0-9]{3})\\)?[-]([0-9]{3})[-]([0-9]{4})$"))
            {
                numValide = true;
            }else
                {
                    System.out.print("Numéro invalide !\n");
                }

        }while (numValide != true);

        return numero;
    }

    /**
     * verifie si le prénom est entre 2 et 30 caract̀é̀res, sinon envoie un message d'erreur
     * @return le prénom de l'utilisateur sans espace
     */

    public static String validationDuPrenom()
    {
        String  prenom = "";
        int nbChara;

        do {
            System.out.print("\nEntrer le prénom : ");
            prenom = Clavier.lireString();
            prenom = prenom.trim();
            nbChara = prenom.length();

            if (nbChara < 2 || nbChara > 30)
            {

                System.out.print("\nNom d'abonné invalide ! ");

            }

        }while (nbChara < 2 || nbChara > 30);

        return prenom;
    }


    /**
     * verifie si le nom est entre 2 et 30 caract̀é̀res, sinon envoie un message d'erreur
     * @return le nom de l'utilisateur.
     */

    public static String validationDuNom()
    {

        String  nom = "";
        int nbChar;

        do {
            System.out.print("\nEntrer le nom : ");
            nom = Clavier.lireString();
            nbChar = nom.length();

            if (nbChar < 2 || nbChar > 30)
            {

                System.out.print("\nNom d'abonné invalide ! ");

            }

        }while (nbChar < 2 || nbChar > 30);

        return nom;
    }

    /**
     * permet le saisi du menu 0, 1 ou 2
     * @return Le choix du menu que le client a fait.
     */

    public static int saisiDuMenu()
    {

        int nbmenu;

        do {
            System.out.print("\nEntrer Choix du menu : ");
            nbmenu = Clavier.lireInt();

            if (nbmenu < 0 || nbmenu > 2)
            {

                System.out.print("\nChoix invalide !");

            }

        }while (nbmenu != 0 && nbmenu !=1 && nbmenu != 2);

        return nbmenu;
    }

    /**
     * Affiche le menu aux trois choix.
     */

    public static void choixDuMenu()
    {

        System.out.print("0. Quitter\n1. Facturation d'un abonnement\n2. Nombre d'abonnés de tous les services");

    }

    /**
     * Affiche le message.
     */

    public static void messageDeBienvenu()
    {

        System.out.println("Bienvenue dans le programme TIPT, qui vous permet de calculer des factures  ");

    }


    public static void main(String[] args)
    {
        int nbMenu;
        messageDeBienvenu();
        do {
        choixDuMenu();
        nbMenu = saisiDuMenu();
        menu(nbMenu);
        }while ( nbMenu != 0);
    }

}
