# Documentation web

Au file de la session je rajouterais de la documentation web qui permetra de mieux utilise les languages web et je rajouterais aussi les tp ici.

## Le HTML

### Balises et Atribus :

Tout les documents sont en francais il faut juste trouver le bouton langues.

[Explication](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_à_HTML/Getting_started) des bases du html balises et Atribus.

L'ensemble des [Balises](https://developer.mozilla.org/fr/docs/Web/HTML/Element) sont regrouper ici. Dans l'onglet Éléments HTML a gauche dans le nav.  
Les [Atribus](https://developer.mozilla.org/fr/docs/Web/HTML/Attributs_universels) sont des elements qu'on insert a l'intérieure des balises pour les spécifier.

Exemple du squelette de base html avec les balises et atribus important. 

```html 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

</body>
</html>

```

[Explication du squelette html](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_à_HTML/The_head_metadata_in_HTML)


### Le \<head\> :

On parle aussi de meta donnée du document

#### liste des balises \<head\> :

* [\<base\>](https://developer.mozilla.org/fr/docs/Web/HTML/Element/base)
* [\<link\>](https://developer.mozilla.org/fr/docs/Web/HTML/Element/link)
* [\<meta\>](https://developer.mozilla.org/fr/docs/Web/HTML/Element/meta)
* [\<style\>](https://developer.mozilla.org/fr/docs/Web/HTML/Element/style)
* [\<title\>](https://developer.mozilla.org/fr/docs/Web/HTML/Element/title)


#### L'encodage 
