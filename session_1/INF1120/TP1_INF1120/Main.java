
/**
 * INF x 120
 *
 *TP1 PARTIE 2
 *
 * ALEXANDRE BILLEREAU
 * DIMANCHE 14 OCTOBRE 2018
 *
 * BILA14069808
 * alexandre.billereau@ens.uqam.ca
 *
 */


package com.company;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        // Declaration des Constantes.

        final float TPS = 0.05f;
        final float TVQ = 0.0975f;


        // Forfait proposer

        final float FORFAIT_TELEPHONE_B = 15.95f;
        final String DESCRIPTION_FORFAITB = "Forfait Bien – choix de 15 chaînes à la carte";
        final float FORFAIT_TELEPHONE_T = 20.95f;
        final String DESCRIPTION_FORFAITT = "Forfait Très Bien– choix de 25 chaînes à la carte";
        final float FORFAIT_TELEPHONE_E = 25.95f;
        final String DESCRIPTION_FORFAITE = "Forfait Excellent – choix de 35 chaînes à la carte";
        final float FORFAIT_INTERNET_30 = 30.95f;
        final String DESCRIPTION_FORFAIT30 = "Internet Fibre hybride 30 - Téléchargement = 30Mb/s et téléversement = 10Mb/s";
        final float FORFAIT_INTERNET_60 = 40.95f;
        final String DESCRIPTION_FORFAIT60 = "Internet Fibre hybride 60 - Téléchargement = 60Mb/s et téléversement = 10Mb/s";
        final float FORFAIT_INTERNET_120 = 50.95f;
        final String DESCRIPTION_FORFAIT120 = "Internet Fibre hybride 60 - Téléchargement = 60Mb/s et téléversement = 10Mb/s";
        final char GRAND_O = 'O';
        final char GRAND_N = 'N';

        float numFacture = 0f;
        char reponse;
        boolean estvalide;

        // info de l'entreprise

        final String TELEPHONE_ENTREPRISE = "(450)-514-0055";
        final String ADRESSE_ENTREPRISE = "2020 rue matata, hakuna, QC";


        // Declaration des variables

        String nom;
        String prenom;
        String numeroDeTelephone;
        String adresseClient;

        // Montant a payer

        float sousTotal = 0f;
        float montantTps = 0f;
        float montantTvq = 0f;
        float montantTotal = 0f;
        String forfaitChoisit1 = "";
        String forfaitChoisit2 = "";
        float prixforfait_t = 0;
        float prixforfait_i = 0;


        // Nieme facture

        int service;
        char forfait_t;
        int forfait_i;

        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d-MM-yyyy HH:mm:ss");
        String laDate = simpleDateFormat.format(date);

        // affichage

        final String MESSAGE_INTERNET = " \nLes trois forfait internet sont :  30 ) " +
                "Téléchargement = 30Mb/s et téléversement = 10Mb/s\n" +
                "                                   60 ) " +
                "Téléchargement = 60Mb/s et téléversement = 10Mb/s\n" +
                "                                   120 ) " +
                "Téléchargement = 120Mb/s et téléversement = 20Mb/s\n ";

        final String MESSAGE_TELE ="\nles forfait tele se décline en trois choix:\n B (bien)  coute : 15,95$\n"+
                " T (tres bien) coute : 20,95$\n E (exelent) cout : 25,95$\n";





        System.out.println(" Ce programme produit pas TIPT, calcule le forfait que vous aller choisir vous avez la" +
                " possibilité de choisir entre le service (1) pour avoir la " +
                "télé (2) pour avoir internet ou (3) pour avoir les deux. \n" +
                "- Trois forfait télé :  B) Forfait Bien – choix de 15 chaînes à la carte\n" +
                "                T) Forfait Bien – choix de 15 chaînes à la carte\n" +
                "                E) Forfait Excellent – choix de 35 chaînes à la carte\n" +
                "- Trois forfait internet :  30 ) Téléchargement = 30Mb/s et téléversement = 10Mb/s\n" +
                "                60 ) Téléchargement = 60Mb/s et téléversement = 10Mb/s\n" +
                "                120 ) Téléchargement = 120Mb/s et téléversement = 20Mb/s\n");

        do
        {

            System.out.print("Voulez vous une nouvelle fiche de calcul (o/n): ");

            reponse = Clavier.lireCharLn();
            reponse = Character.toUpperCase(reponse);



            if (reponse =='O') {

                numFacture = numFacture + 1;

                System.out.print("Entrer votre nom : ");
                nom = Clavier.lireString();
                System.out.print("Entrer votre prénom : ");
                prenom = Clavier.lireString();
                System.out.print("Entrer votre numéro de téléphone : ");
                numeroDeTelephone = Clavier.lireString();
                System.out.print("Entrer votre adresse : ");
                adresseClient = Clavier.lireString();

                do{
                     System.out.print("Choisir votre forfait télévision (1), internet (2) ou les deux (3) : ");
                     service = Clavier.lireIntLn();



                    if (service == 1 || service == 3) {
                        System.out.println(MESSAGE_TELE);


                        do {
                            System.out.print("\nChoisir selon les forfait proposé : ");
                            forfait_t = Clavier.lireCharLn();

                            switch (forfait_t) {

                                case 'B':

                                    forfaitChoisit2 = DESCRIPTION_FORFAITB;
                                    prixforfait_t = FORFAIT_TELEPHONE_B;
                                    sousTotal = FORFAIT_TELEPHONE_B;
                                    montantTps = sousTotal * TPS;
                                    montantTvq = sousTotal * TVQ;
                                    montantTotal = montantTps + montantTvq + sousTotal;
                                    break;

                                case 'T':

                                    forfaitChoisit2 = DESCRIPTION_FORFAITT;
                                    prixforfait_t = FORFAIT_TELEPHONE_T;
                                    sousTotal = FORFAIT_TELEPHONE_T;
                                    montantTps = sousTotal * TPS;
                                    montantTvq = sousTotal * TVQ;
                                    montantTotal = montantTps + montantTvq + sousTotal;
                                    break;

                                case 'E':

                                    forfaitChoisit2 = DESCRIPTION_FORFAITE;
                                    prixforfait_t = FORFAIT_TELEPHONE_E;
                                    sousTotal = FORFAIT_TELEPHONE_E;
                                    montantTps = sousTotal * TPS;
                                    montantTvq = sousTotal * TVQ;
                                    montantTotal = montantTps + montantTvq + sousTotal;
                                    break;
                            }


                        } while (forfait_t != 'B' && forfait_t != 'T' && forfait_t != 'E');
                    }

                    if (service == 2 || service == 3) {

                        System.out.println(MESSAGE_INTERNET);

                        do {
                            System.out.print("\nChoisir selon les forfait proposé :");
                            forfait_i = Clavier.lireInt();

                            switch (forfait_i) {

                                case 30:
                                    forfaitChoisit1 = DESCRIPTION_FORFAIT30;
                                    prixforfait_i = FORFAIT_INTERNET_30;
                                    sousTotal = FORFAIT_INTERNET_30 + prixforfait_t;
                                    montantTps = sousTotal * TPS;
                                    montantTvq = sousTotal * TVQ;
                                    montantTotal = montantTps + montantTvq + sousTotal;
                                    break;

                                case 60:
                                    forfaitChoisit1 = DESCRIPTION_FORFAIT60;
                                    prixforfait_i = FORFAIT_INTERNET_60;
                                    sousTotal = FORFAIT_INTERNET_60 + prixforfait_t;
                                    montantTps = sousTotal * TPS;
                                    montantTvq = sousTotal * TVQ;
                                    montantTotal = montantTps + montantTvq + sousTotal;
                                    break;

                                case 120:
                                    forfaitChoisit1 = DESCRIPTION_FORFAIT120;
                                    prixforfait_i = FORFAIT_INTERNET_120;
                                    sousTotal = FORFAIT_INTERNET_120 + prixforfait_t;
                                    montantTps = sousTotal * TPS;
                                    montantTvq = sousTotal * TVQ;
                                    montantTotal = montantTps + montantTvq + sousTotal;
                                    break;

                            }
                        } while (forfait_i != 30 && forfait_i != 60 && forfait_i != 120);

                    }

                }while (service!= 1 && service != 2 && service != 3 );

                    System.out.println("<------------------------------------- Télé & Internet Pour Tous " +
                            "----------------------------------->\n");
                    System.out.print(ADRESSE_ENTREPRISE + "                                            " +
                            "      " + TELEPHONE_ENTREPRISE + "\n");
                    System.out.print("-----------------------------------------------------------------------------" +
                            "------------------------\n");
                    System.out.print( "Date :" + laDate + "                                                     " +
                            "Facture no :  " + numFacture + "\n\nnom et prénom : " + nom + " " + prenom + "\n");
                    System.out.print("      Adresse : " + adresseClient + "                                       " +
                            "    " + "Téléphone : " + numeroDeTelephone + "\n\n");
                    System.out.print("Description                                               " +
                            "          Prix\n");
                    System.out.print(forfaitChoisit2 + "                    " +
                            " " + prixforfait_t + "\n");
                    System.out.print(forfaitChoisit1 + "                    " +
                            " " + prixforfait_i + "\n");
                    System.out.printf("\nsous-total : %2.2f\n", sousTotal);
                    System.out.printf("Montant TPS : %2.2f\n", montantTps);
                    System.out.printf("Montant TVQ : %2.2f\n", montantTvq);
                    System.out.printf("Montant total : %2.2f\n", montantTotal);
                    System.out.println("Merci d'avoir choisit TIPT !\n\n");



            }

            else if (reponse == 'N')
            {

                System.out.println("merci encore de nous avoir choisit");
            }


        }while (reponse != 'N');

    }
}